-- Comment in SQL

drop table clients; -- удаление таблицы clients

-- constraints (ограничения)
-- not null - значение в колонке не может быть null
-- unique - в колонке не может быть дубликатов
-- может вешаться на несколько колонок сразу
-- 4012 2345
-- 4012 2346
-- 2342 2345
-- primary key - первичный ключ - уникальный идентификатор строки - unique not null
-- deafult <value>
-- check <condition>
-- foreign key
create table clients (
    -- varchar ~ java.lang.String
                         client_id serial primary key,
                         name varchar not null,
                         last_name varchar not null,
                         passport_seria varchar not null,
                         passport_number varchar not null,
                         birthday date,
    -- unique(passport_seria, passport_number)
    -- constraint unique_passport unique(passport_seria, passport_number)
                         constraint unique_passport unique(passport_seria, passport_number)
);


alter table clients drop constraint unique_passport;
alter table clients add constraint unique_passport_key unique(passport_seria, passport_number)

alter table clients drop column address;
alter table clients add column address varchar;
-- update data
alter table clients alter column address set not null;


-- Add data
-- insert into <table_name> values ()
insert into clients (passport_seria, passport_number, last_name, name)
values
    ('4010', '487234', 'Ivanov', 'Vladimir'),
    ('4011', '465756', 'Volkov', 'Ivan');

-- select <columns> from <table>
select * from clients;

alter table clients add column address varchar not null default 'no-address';

insert into clients (passport_seria, passport_number, last_name, name)
values
    ('4011', '592932', 'Sidorov', 'Petr'),
    ('4011', '887567', 'Petrov', 'Oleg'),
    ('4003', '871523', 'Rek', 'Olga'),
    ('4001', '684562', 'Soldatov', 'Michael'),
    ('4003', '585487', 'Vilkov', 'Alexandr');

--
select last_name, name, passport_seria, passport_number from clients;

select * from clients
where passport_seria = '4011';

select * from clients
-- like 'S%' - найти все строки начинающиеся на S
-- like '%S' - найти все строки заканчивающиеся на S
-- like '%S%' - найти все строки, имеющие символ S
-- like 'S' ~  = 'S'
where last_name like 'S%';

select * from clients
where birthday is not null;

select * from clients
where passport_seria = '4003' or passport_seria = '4011';

select * from clients
-- in/not in
where passport_seria not in ('4001', '4003', '4010');

-- update/delete
-- update <table> set <col1>=<val1> [where <conditions>]
update clients set birthday = '1995-03-01' where client_id = 4;
update clients set birthday = '1991-09-23', last_name = 'Bogatireva' where client_id = 5;

-- delete from <table> [where <condtions>]
-- delete from clients where passport_seria = '4010';

