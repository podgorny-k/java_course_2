select * from clients c, managers m
where c.manager_id = m.manager_id;

-- cross join
-- 1. Все данные из таблицы clients
-- 2. Все данные из таблицы managers
-- 3. Декартово произведение
-- 4. Фильтрация большой таблицы с использованием where

select * from clients c
-- join ~ inner join
                  inner join managers m on c.manager_id = m.manager_id
                  inner join client_info ci on c.client_id = ci.client_id
where c.passport_seria = '4011';


select * from clients c
-- left outer join ~ left join
                  left outer join managers m on c.manager_id = m.manager_id
where m.manager_id is null;

-- right join
select * from clients c
                  right join managers m on c.manager_id = m.manager_id
where c.client_id is null;

select * from managers m
left join clients c on c.manager_id = m.manager_id
where c.client_id is null;


-- Aggregate functions
-- max(), min(), sum(), count(), avg()
-- count(*) vs count(column_name)
select * from clients;
select count(*) from clients;			-- 7
select count(manager_id) from clients; 	-- 6

select * from client_info ;
select min(birthday) from client_info;

select * from managers m
left join clients c on c.manager_id = m.manager_id

-- m.name, m.last_name, count of clients
select m.name, m.last_name, count(c.manager_id) from managers m
left join clients c on c.manager_id = m.manager_id
group by m.name, m.last_name;

select m.name, m.last_name, count(c.manager_id) from managers m
left join clients c on c.manager_id = m.manager_id
group by m.manager_id;

