create table client_info (
    -- primary key - client_id
                             client_id bigint primary key,
                             contact_number varchar not null,
                             birthday date not null,
                             address varchar,
                             sex int not null,
                             constraint client_info_client_id_fkey foreign key (client_id) references clients(client_id)
);

insert into client_info
values
    (1, '89452543452', '1991-04-23', null, 1);

select * from client_info;

create table managers (
                          manager_id serial primary key,
                          name varchar not null,
                          last_name varchar not null,
                          contact_number varchar not null unique
);

insert into managers (name, last_name, contact_number)
values
    ('Vladimir', 'Troyanov', '89331235423');

-- Удалить 2 колонки (birthday, address)
-- Добавить колонку manager_id
-- Добавить связь 1-М между таблицами clients и managers
alter table clients drop column birthday;
alter table clients drop column address;

alter table clients add column manager_id bigint;

alter table clients add constraint clients_manager_id_fkey foreign key (manager_id) references managers(manager_id);

create table accounts (
    account_id serial primary key,
    account_number varchar not null unique,
    open_date timestamp not null
);

create table clients_accounts (
  client_id bigint not null,
  account_id bigint not null,
  primary key (client_id, account_id),
  foreign key (client_id) references clients(client_id),
  foreign key (account_id) references accounts(account_id)
);


