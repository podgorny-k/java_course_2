-- Запрос на то, сколько клиентов у каждого менеджера
-- Порядок выполнения запроса
-- 1. Joins
-- 2. Where
-- 3. Group by
-- 4. Having
-- 5. Order by
-- 6. Limit offset

select m.name, m.last_name, count(c.manager_id) from managers m
                                                         left join clients c on c.manager_id = m.manager_id
group by m.manager_id
having count(c.manager_id) > 1
order by count
    limit 1 offset 1;


-- serial type - bigint + auto_increment
-- insert into managers (manager_id, name, last_number, contact_number) values (....)
-- nextval

-- create sequence <table_column_seq> increment by 1

create tables managers(
	-- manager_id serial primary key
	manager_id bigint primary key default nextval('seq_name'),
	name varchar not null
);

create sequence test_sequence
    increment by 13
    start with 1000;

-- nextval('<seq_name>') - next value of sequence
-- currval('<seq_name') - current value
-- setval('<seq_name>', value)

select nextval('test_sequence');
select setval('test_sequence', 1001);
select currval('test_sequence');

insert into client_info
values
    (2, '89214568799', '1990-02-23', null, nextval('test_sequence'));

update client_info set sex = 1 where client_id = 2;
select * from client_info;

