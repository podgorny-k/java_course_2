package org.levelup.threads;

public class CounterApplication {

    public static void main(String[] args) throws InterruptedException {

        Counter counter = new NonBlockingCounter();
        Runnable worker = () -> {
            try {
                for (int i = 0; i < 30; i++) {
                    Thread.sleep(100);
                    counter.increment();
                }
            } catch (InterruptedException exc) {
                System.out.println("Thread has been interrupted");
            }
        };

        Thread t1 = new Thread(worker, "thread-1");
        Thread t2 = new Thread(worker, "thread-2");
        Thread t3 = new Thread(worker, "thread-3");

        t1.start();
        t2.start();
        t3.start();

        t1.join();
        t2.join();
        t3.join();

        System.out.println("Result: " + counter.getValue());
    }

}
