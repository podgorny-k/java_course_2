package org.levelup.threads;

import java.util.concurrent.atomic.AtomicInteger;

public class NonBlockingCounter implements Counter {

    // CAS (compare and swap (set))
    private final AtomicInteger counter = new AtomicInteger(0);

    @Override
    public void increment() {
        counter.incrementAndGet();
    }

    @Override
    public int getValue() {
        return counter.get();
    }

}
