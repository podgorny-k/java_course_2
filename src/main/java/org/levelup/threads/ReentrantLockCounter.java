package org.levelup.threads;

import java.util.concurrent.locks.ReentrantLock;

// 3 lock types:
// 1. biased - one CAS at all
// 2. thin   - thread changing occurs using CAS
// 3. fat    - context's change
public class ReentrantLockCounter implements Counter {

    private final ReentrantLock reentrantLock = new ReentrantLock();
    private int counter;

    @Override
    public void increment() {
//        if (!reentrantLock.tryLock()) {
//            // что-то сделать, если блокировка взята до вас
//        }
        reentrantLock.lock();
        // ...
        // reentrantLock.unlock();
        try {
            counter++;
        } finally {
            reentrantLock.unlock();
        }
    }

    @Override
    public int getValue() {
        return counter;
    }
}
