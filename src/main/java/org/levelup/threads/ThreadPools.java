package org.levelup.threads;

import java.util.Random;
import java.util.concurrent.*;

public class ThreadPools {

    public static void main(String[] args) throws InterruptedException  {

        // ExecutorService pool = Executors.newSingleThreadExecutor();  // создаем пул из одного потока
        // ExecutorService pool = Executors.newFixedThreadPool(10);     // создаем пул из десяти потоков
        // ExecutorService pool = Executors.newCachedThreadPool();      // создаем пул от 0 до Integer.MAX_VALUE
        ExecutorService pool = new ThreadPoolExecutor(
                3,
                15,
                60L,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>()
        );

//        CompletableFuture.supplyAsync(() -> {
//                            System.out.println("");
//                            return 3;
//                        }, pool)
//                        .thenAcceptAsync(() -> {}, pool);

        pool.submit(() -> System.out.println("Hello from runnable from pool"));

        Future<Integer> futureResult = pool.submit(() -> {
            try {
                Thread.sleep(2000);
                return new Random().nextInt(40);
            } catch (InterruptedException exc) {
                return null;
            }
        });
        try {
            // futureResult.cancel(false);
            Integer result = futureResult.get(); // поток, в котором вызван метод get блокируется пока не получит результат из Future
            System.out.println(result);
        } catch (InterruptedException | ExecutionException exc) {
            System.out.println();
        }

        // } catch (InterruptedException exc) {
//      //    System.out.println();
//      // } catch (ExecutionException exc) {
        //    System.out.println();
        // }

        pool.shutdown();

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(5);
        System.out.println("Before scheduling task...");
        scheduler.schedule(() -> System.out.println("Hello after 5 seconds"), 5L, TimeUnit.SECONDS);

        scheduler.scheduleAtFixedRate(() -> System.out.println("Each 2 seconds"), 1L, 2L, TimeUnit.SECONDS);
        scheduler.scheduleWithFixedDelay(() -> System.out.println("With delay 2 seconds"), 4, 3, TimeUnit.SECONDS);

        Thread.sleep(30_000);
        scheduler.shutdown();
    }

}
