package org.levelup.threads;

public interface Counter {

    void increment();

    int getValue();

}
