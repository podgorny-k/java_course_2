package org.levelup.first.maven.comparison;

import javax.swing.text.html.Option;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("ALL")
public class StreamsExample {

    public static void main(String[] args) {

        List<Account> accounts = new ArrayList<>();
        accounts.add(new Account(450L, "1230490954", LocalDateTime.now()));
        accounts.add(new Account(423L, "5492344332", LocalDateTime.now()));
        accounts.add(new Account(112L, "2350934934", LocalDateTime.now()));
        accounts.add(new Account(153L, "3142524324", LocalDateTime.now()));
        accounts.add(new Account(243L, "1234568654", LocalDateTime.now()));

        // Отфильтровать коллекцию, оставить только те счета, который начинаются на 3 или 5
        List<Account> filteredAccountNumbers = new ArrayList<>();
        for (Account account : accounts) {
            if (account.getAccountNumber().startsWith("3") || account.getAccountNumber().startsWith("5")) {
                filteredAccountNumbers.add(account);
            }
        }
        for (Account filteredAccountNumber : filteredAccountNumbers) {
            System.out.println(filteredAccountNumber);
        }

        System.out.println();

        List<Account> filtered = accounts.stream()
                .filter(Objects::nonNull)
                .filter(acc -> acc.getAccountNumber().startsWith("3") || acc.getAccountNumber().startsWith("5"))
                .collect(Collectors.toList());
        // filtered.forEach(acc -> System.out.println(acc));
        filtered.forEach(System.out::println);

        System.out.println();
        // Вывести список accountId, которые больше 200 и отсортированные по возрастанию
        List<Long> sortedAndFilteredAccountIds = accounts.stream()
                // .map(account -> account.getAccountId()) // Stream<Long>
                .map(Account::getAccountId) // Stream<Long>
                .filter(accountId -> accountId > 200L)
                .sorted()

                .collect(Collectors.toList());
        sortedAndFilteredAccountIds.forEach(accId -> System.out.println(accId));


        System.out.println();
        // Stream, IntStream, DoubleStream, LongStream
        // Stream<Long> vs LongStream
        OptionalLong possibleMax = accounts.stream()
                .mapToLong(Account::getAccountId)
                .max();
        possibleMax.ifPresentOrElse(
                max -> System.out.println(max),
                () -> System.out.println("No max: collection is empty")
        );

        System.out.println();
        Optional<Account> optionalAccount = Optional.ofNullable(new Account(34L, "12341235413", LocalDateTime.now()));
        Optional<Account> emptyOptionalAccount = Optional.ofNullable(null); // Optional.empty();

        Account acc1 = optionalAccount.orElse(new Account(0L, "", LocalDateTime.now()));
        Account acc2 = emptyOptionalAccount.orElse(new Account(0L, "", LocalDateTime.now()));

        LocalDateTime accountOpenDate = optionalAccount
                .map(Account::getOpenDate)
                .orElse(null);
        optionalAccount.ifPresent(acc -> System.out.println(acc));

        System.out.println();
        List<String> list = new ArrayList<>();
        list.add("132");
        list.add("534");
        list.add("234");
        list.add("985");
        list.add("875");
        list.add("456");

        long count = list.stream()
                // .map(val -> Integer.parseInt(val))
                .map(Integer::parseInt)
                .filter(val -> val > 400)
                .count();
        System.out.println(count);

        // Class::method
        //   obj -> obj.method() - Class::method
        //   obj -> Class.method(obj) - Class::method

        // object::method
        //   obj1 -> obj2.method(obj1) - obj2::method


        //
        List<Account> accList1 = List.of(
                new Account(13L, "345345", null),
                new Account(16L, "436755", null)
        );
        List<Account> accList2 = List.of(
                new Account(56L, "23445245", null),
                new Account(76L, "2345524", null),
                new Account(123L, "54362345", null)
        );
        List<Account> accList3 = List.of(
                new Account(656L, "34523245", null)
        );

        List<Client> clients = List.of(
            new Client(53L, accList1),
            new Client(36L, accList2),
            new Client(65L, accList3)
        );


        List<String> accountNumbers = clients.stream()
                // Stream<Client> -> Stream<List<Account>> -> Stream<Account>
                .flatMap(client -> client.getAccounts().stream()) // Stream<List<Account>> [[accList1], [accList2], [accList3]]
                .map(Account::getAccountNumber)
                .collect(Collectors.toList());
        accountNumbers.forEach(System.out::println);


    }

}
