package org.levelup.first.maven.comparison;

import java.util.function.Predicate;

public class StringLengthPredicate implements Predicate<Account> {

    private int maxLength;

    public StringLengthPredicate(int maxLength) {
        this.maxLength = maxLength;
    }

    @Override
    public boolean test(Account a) {
        return a.getAccountNumber().length() < maxLength;
    }

}
