package org.levelup.first.maven.cme;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class FileLoop {

    public static void main(String[] args) {
        String packageName = "src/main/java/org/levelup/";
        String packageName2 = "target/";

        Set<String> path = new HashSet<>();
        path.add(packageName);
        path.add(packageName2);
        for (String s : path) {
            File files = new File(s);
            for (File file : files.listFiles()) {
                if (file.isDirectory()) {
                    path.add(file.getPath());
                }
            }
        }

        System.out.println(path);
    }

}
