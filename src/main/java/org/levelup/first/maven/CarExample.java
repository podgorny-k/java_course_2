package org.levelup.first.maven;

public class CarExample {

    public static void main(String[] args) {
        Car car = Car.builder()
                .brand("")
                .model("")
                .price(43.d)
                .build();
    }

}
