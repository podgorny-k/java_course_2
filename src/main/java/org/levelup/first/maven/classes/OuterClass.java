package org.levelup.first.maven.classes;

public class OuterClass {

    private int privateIntVariable;

    private static int privateStaticIntVariable;

    // Внутренний класс может иметь любой модификатор доступа
    public class InnerClass {

        private int innerClassVariable;

        public void changeIntVariable(int var) {
            privateIntVariable = var;
            privateStaticIntVariable = var;
        }

        public class DoubleInnerClass {

        }

    }

    // Вложенный класс
    public static class NestedClass {

        public void changeStaticIntVariable(int var) {
            privateStaticIntVariable = var;
        }

    }


}
