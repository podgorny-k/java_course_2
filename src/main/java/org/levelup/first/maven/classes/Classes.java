package org.levelup.first.maven.classes;

public class Classes {

    public static void main(String[] args) {

        OuterClass outerClass = new OuterClass();

        OuterClass.InnerClass innerClass1 = outerClass.new InnerClass();
        OuterClass.InnerClass innerClass2 = outerClass.new InnerClass();

        OuterClass.InnerClass innerClass3 = new OuterClass().new InnerClass();

        OuterClass.NestedClass nestedClass = new OuterClass.NestedClass();
        nestedClass.changeStaticIntVariable(45);

        OuterClass outerClass2 = new OuterClass();

        OuterClass.InnerClass.DoubleInnerClass doubleInnerClass =
                new OuterClass().new InnerClass().new DoubleInnerClass();

    }

}
