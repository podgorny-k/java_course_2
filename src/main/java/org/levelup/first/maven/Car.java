package org.levelup.first.maven;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
// @JsonDeserializer(builderClass = Car.CarBuilder.class)
public class Car {

    String model;
    String brand;
    double price;

    // @JSON...
    public static class CarBuilder {}

}
