package org.levelup.banking.jdbc;

import org.levelup.banking.domain.Client;
import org.levelup.banking.jdbc.pool.JdbcConnectionService;
import org.levelup.banking.repository.ClientRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Класс для доступа к данным таблицы clients через JDBC
 */
public class JdbcClientRepository implements ClientRepository {

    private final JdbcConnectionService jdbcConnectionService;        // class dependency

    public JdbcClientRepository(JdbcConnectionService jdbcConnectionService) {
        this.jdbcConnectionService = jdbcConnectionService;
        // this.jdbcConnectionService = new JdbcConnectionService();
    }

    @Override
    public Client create(String name, String lastName, String passportSeries, String passportNumber) {
        try (Connection connection = jdbcConnectionService.openNewConnection()) {
            // select * from client where last_name = "\" \"; drop table clients;"
            // ... = ' ; drop table clients'
            PreparedStatement stmt = connection.prepareStatement(
                    "insert into clients (name, last_name, passport_seria, passport_number) values (?, ?, ? ,?)",
                    Statement.RETURN_GENERATED_KEYS
            );
            stmt.setString(1, name);
            stmt.setString(2, lastName);
            stmt.setString(3, passportSeries);
            stmt.setString(4, passportNumber);

            int rowsAffected = stmt.executeUpdate(); // insert/update/delete выполняются через метод executeUpdate();
            System.out.println("Rows inserted: " + rowsAffected);

            ResultSet resultSet = stmt.getGeneratedKeys();
            resultSet.next();
            long clientId = resultSet.getLong("client_id");

            return new Client(clientId, name, lastName, passportSeries, passportNumber);

        } catch (SQLException exc) {
            System.err.println("An SQL error occurred during creating client: " + exc.getMessage());
            throw new RuntimeException(exc);
        }
    }

    @Override
    public Collection<Client> findAll() {
        try (Connection connection = jdbcConnectionService.openNewConnection()) {
            Statement stmt = connection.createStatement();

            ResultSet resultSet = stmt.executeQuery("select * from clients");

            Collection<Client> clients = new ArrayList<>();
            while (resultSet.next())  {
                long clientId = resultSet.getLong("client_id");
                String name = resultSet.getString("name");
                String lastName = resultSet.getString("last_name");
                String series = resultSet.getString("passport_seria");
                String number = resultSet.getString("passport_number");

                Client client = new Client(clientId, name, lastName, series, number);
                clients.add(client);
            }

            return clients;
        } catch (SQLException exc) {
            System.err.println("An SQL error occurred during getting all clients: " + exc.getMessage());
            throw new RuntimeException(exc);
        }
    }

    @Override
    public Client assignManager(Long clientId, Long managerId) {
        return null;
    }

}
