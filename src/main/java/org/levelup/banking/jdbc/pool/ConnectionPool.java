package org.levelup.banking.jdbc.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.IdentityHashMap;
import java.util.Set;

// Пул соединений к базе
public class ConnectionPool {

    private IdentityHashMap<Connection, Boolean> pool;

    public ConnectionPool() {
        this.pool = new IdentityHashMap<>();
    }

    void createConnectionInPool(Connection connection) {
        pool.put(connection, true);
    }

    Connection getConnection() {
        Set<Connection> connections = pool.keySet();
        for (Connection connection : connections) {
            Boolean isAllowed = pool.get(connection);
            if (isAllowed != null && isAllowed) {
                pool.put(connection, false);        // говорим, что соединение занято
                return connection;
            }
        }
        // Возвращаем null, если в пуле нет доступных соединений
        return null;
    }

    void returnConnection(Connection connection) {
        pool.put(connection, true);
    }

}
