package org.levelup.banking.repository.hbm;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.levelup.banking.domain.Account;
import org.levelup.banking.domain.Client;
import org.levelup.banking.repository.AccountRepository;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RequiredArgsConstructor
public class HbmAccountRepository implements AccountRepository {

    private final SessionFactory factory;

    @Override
    public Account createAccount(String accountNumber, Set<Long> clientIds) {
        try (Session session = factory.openSession()) {
            Transaction tx = session.beginTransaction();

            Account account = new Account();
            account.setAccountNumber(accountNumber);
            account.setOpenDate(OffsetDateTime.now(ZoneId.of("UTC")).toLocalDateTime()); // текущее время в UTC

            List<Client> clients = session.createQuery("from Client where clientId in (:clientIds)", Client.class)
                            .setParameter("clientIds", clientIds)
                            .getResultList();
            account.setAssignedClients(clients);
            session.persist(account);

            for (Client client : clients) {
                client.setAccounts(List.of(account)); // java 9+
            }

            tx.commit();
            return account;
        }
    }

}
