package org.levelup.banking.repository.hbm;

import org.hibernate.SessionFactory;
import org.levelup.banking.domain.Client;
import org.levelup.banking.domain.Manager;
import org.levelup.banking.repository.ClientRepository;

import java.util.Collection;

public class HbmClientRepository extends HbmAbstractRepository implements ClientRepository {

    public HbmClientRepository(SessionFactory factory) {
        super(factory);
    }

    @Override
    public Client create(String name, String lastName, String passportSeries, String passportNumber) {
        return null;
    }

    @Override
    public Client assignManager(Long clientId, Long managerId) {
        return runInTransaction(session -> {
            Client client = session.get(Client.class, clientId); // select * from clients where client_id = :clientId
            Manager manager = session.load(Manager.class, managerId); // select * from managers where manager_id = :managerId
            client.setManager(manager); // client - persistent
            return client;
        });
//        try (Session session = factory.openSession()) {
//            Transaction tx = session.beginTransaction();
//
//            Client client = session.get(Client.class, clientId); // select * from clients where client_id = :clientId
//            Manager manager = session.load(Manager.class, managerId); // select * from managers where manager_id = :managerId
//
//            client.setManager(manager); // client - persistent
//            tx.commit();
//
//            return client;
//        }
    }

    @Override
    public Collection<Client> findAll() {
        return null;
    }

}
