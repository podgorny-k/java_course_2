package org.levelup.banking.repository.hbm;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.levelup.banking.domain.Manager;
import org.levelup.banking.repository.ManagerRepository;

import java.util.List;

@RequiredArgsConstructor
// public HbmManagerRepository(SessionFactory factory) { this.factory = factory; }
public class HbmManagerRepository implements ManagerRepository  {

    private final SessionFactory factory;

    @Override
    public Manager create(String name, String lastName, String contactNumber) {
        try (Session session = factory.openSession()) {
            Transaction transaction = session.beginTransaction();
            // Начали транзакцию

            Manager manager = new Manager(); // transient
            manager.setName(name);
            manager.setLastName(lastName);
            manager.setContactNumber(contactNumber);

            session.save(manager); // persistent
            // session.persist(manager);

            transaction.commit(); // Фиксация транзакции
            return manager;
        }
        // manager - detached
    }

    @Override
    public List<Manager> findAll() {
        try (Session session = factory.openSession()) {
            // HQL - Hibernate Query Language
            return session.createQuery("from Manager", Manager.class)
                    .getResultList();
        }
    }

    @Override
    public Manager findByContactNumber(String contactNumber) {
        try (Session session = factory.openSession()) {
            // HQL - Hibernate Query Language
            List<Manager> list = session.createQuery("from Manager where contactNumber = :cn", Manager.class)
                    .setParameter("cn", contactNumber)
                    .getResultList();
            return list.isEmpty() ? null : list.get(0);
        }
    }

}
