package org.levelup.banking.repository;

import org.levelup.banking.domain.Account;

import java.util.Set;

public interface AccountRepository {

    Account createAccount(String accountNumber, Set<Long> clientIds);

}
