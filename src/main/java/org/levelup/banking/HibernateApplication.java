package org.levelup.banking;

import org.hibernate.SessionFactory;
import org.levelup.banking.domain.Account;
import org.levelup.banking.domain.Client;
import org.levelup.banking.domain.Manager;
import org.levelup.banking.hbm.HibernateConfiguration;
import org.levelup.banking.repository.AccountRepository;
import org.levelup.banking.repository.ClientRepository;
import org.levelup.banking.repository.ManagerRepository;
import org.levelup.banking.repository.hbm.HbmAccountRepository;
import org.levelup.banking.repository.hbm.HbmClientRepository;
import org.levelup.banking.repository.hbm.HbmManagerRepository;

import java.util.List;
import java.util.Set;

public class HibernateApplication {

    public static void main(String[] args) {
        HibernateConfiguration.configureSessionFactory();
        SessionFactory factory = HibernateConfiguration.getFactory();

        ManagerRepository managerRepository = new HbmManagerRepository(factory);
        // Manager manager = managerRepository.create("Aleksey", "Vladimov", "+79324334345");
        // System.out.println(manager);

        System.out.println("List of all managers");
        List<Manager> managers = managerRepository.findAll();
        for (Manager manager : managers) {
            System.out.println(manager);
        }

        System.out.println();
        System.out.println();
        Manager manager = managerRepository.findByContactNumber("+79324334345");
        System.out.println(manager);

        System.out.println("\n\n\n\n");
        ClientRepository clientRepository = new HbmClientRepository(factory);
//        System.out.println("Assign manager");
//        Client client = clientRepository.assignManager(7L, 6L);
//        System.out.println(client);

        AccountRepository accountRepository = new HbmAccountRepository(factory);
        Account account = accountRepository.createAccount("8475234104565667", Set.of(11L));
        System.out.println(account);

        factory.close();
    }

}
