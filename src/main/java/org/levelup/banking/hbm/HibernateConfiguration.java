package org.levelup.banking.hbm;

import lombok.Getter;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.levelup.banking.domain.Account;
import org.levelup.banking.domain.Client;
import org.levelup.banking.domain.Manager;

import java.util.Properties;

public class HibernateConfiguration {

    @Getter
    private static SessionFactory factory;

    public static void configureSessionFactory() {
        Properties properties = new Properties();

        properties.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");
        properties.setProperty("hibernate.connection.url", "jdbc:postgresql://127.0.0.1:5432/banks");
        properties.setProperty("hibernate.connection.username", "postgres");
        properties.setProperty("hibernate.connection.password", "root");

        properties.setProperty("hibernate.show_sql", "true");
        properties.setProperty("hibernate.format_sql", "true");

        Configuration configuration = new Configuration();

        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .applySettings(properties)
                .build();

        factory = configuration
                .addAnnotatedClass(Manager.class)
                .addAnnotatedClass(Client.class)
                .addAnnotatedClass(Account.class)
                .buildSessionFactory(registry);
    }

}
