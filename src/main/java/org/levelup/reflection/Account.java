package org.levelup.reflection;

import org.levelup.reflection.ioc.RandomInt;

public class Account {

    // @RandomInt(min = 10, max = 20)
    private String accountNumber;
    private double amount;

    @RandomInt(min = 10, max = 20)
    private int currencyCode;

    public int getCurrencyCode() {
        return currencyCode;
    }
}
