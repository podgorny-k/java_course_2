package org.levelup.reflection.ioc;

import org.levelup.reflection.Account;

public class IocApplication {

    public static void main(String[] args) {
        Account account = Context.getObject(Account.class);
        System.out.println(account.getCurrencyCode());
    }

}
