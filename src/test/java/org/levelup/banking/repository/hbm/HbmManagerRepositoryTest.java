package org.levelup.banking.repository.hbm;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.levelup.banking.domain.Manager;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class HbmManagerRepositoryTest {

    // @BeforeAll - public static void method()
    // @BeforeEach
    // @Test
    // @AfterEach
    // @BeforeEach
    // @Test
    // @AfterEach
    // @AfterAll

    private SessionFactory factory;
    private Session session;
    private Transaction tx;

    private HbmManagerRepository managerRepository; // то, что тестируем

    @BeforeEach
    public void setup() {
        // create mocks
        factory = mock(SessionFactory.class);
        session = mock(Session.class);
        tx = mock(Transaction.class);

        // configure mocks
        when(factory.openSession()).thenReturn(session);
        when(session.beginTransaction()).thenReturn(tx);

        // init
        managerRepository = new HbmManagerRepository(factory);
    }

    @Test
    public void testCreate_whenInputIsValid_thenCreateManager() {
        // given
        String expectedName = "name";
        String expectedLastName = "last_name";
        String expectedContactNumber = "contact_number";

        // when
        Manager result = managerRepository.create(expectedName, expectedLastName, expectedContactNumber);

        // then
        assertEquals(expectedName, result.getName());
        assertEquals(expectedLastName, result.getLastName());
        assertEquals(expectedContactNumber, result.getContactNumber());

        // verify
        verify(tx).commit();
        verify(session).close();
        // Mockito.verify(tx, Mockito.times(1)).commit();
    }

    @Test
    public void testFindAll_whenManagersExist_thenReturnListOfManagers() {
        // given
        List<Manager> managers = List.of(
                new Manager(),
                new Manager(),
                new Manager()
        );

        // Mockito.when(obj.smth(1)).thenReturn(...);
        // obj.smth(2) -> interceptor won't work
        Query query = mock(Query.class);
        when(session.createQuery(anyString(), eq(Manager.class))).thenReturn(query);
        when(query.getResultList()).thenReturn(managers);

        // when
        List<Manager> result = managerRepository.findAll();

        // then
        assertSame(managers, result);
    }

}