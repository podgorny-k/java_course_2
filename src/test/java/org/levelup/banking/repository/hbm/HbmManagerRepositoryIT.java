package org.levelup.banking.repository.hbm;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.*;
import org.levelup.banking.domain.Manager;
import org.levelup.banking.repository.ManagerRepository;

import java.util.List;

public class HbmManagerRepositoryIT {

    private static ManagerRepository managerRepository;

    @BeforeAll
    public static void initializeSessionFactory() {
        HibernateTestConfiguration.configureSessionFactory();

        managerRepository = new HbmManagerRepository(HibernateTestConfiguration.getFactory());
    }

    @Test
    public void testCreate() {
        // given
        String name = "manager_name";
        String lastName = "manager_last_name";
        String contactNumber = "manager_contact_number";

        // when
        Manager result = managerRepository.create(name, lastName, contactNumber);

        // then
        Assertions.assertNotNull(result.getId());

        Session session = HibernateTestConfiguration.getFactory().openSession();
        Manager fromDb = session.get(Manager.class, result.getId());

        Assertions.assertEquals(name, fromDb.getName());
        Assertions.assertEquals(lastName, fromDb.getLastName());
        Assertions.assertEquals(contactNumber, fromDb.getContactNumber());

        session.close();
    }

    @Test
    public void testFindAll_whenManagersExist_thenReturnNotEmptyList() {
        // given
        managerRepository.create("name", "last_name", "contact_number");

        // when
        List<Manager> result = managerRepository.findAll();

        // then
        Assertions.assertFalse(result.isEmpty());
        result.forEach(manager -> {
            Assertions.assertNotNull(manager);
            Assertions.assertNotNull(manager.getId());
        });
    }

    @Test
    public void testFindAll_whenManagersTableIsEmpty_thenReturnEmptyList() {
        // given
        Session session = HibernateTestConfiguration.getFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.createQuery("delete from Manager").executeUpdate();
        tx.commit();
        session.close();

        // when
        List<Manager> result = managerRepository.findAll();

        // then
        Assertions.assertTrue(result.isEmpty());
    }

    @AfterAll
    public static void shutdownSessionFactory() {
        HibernateTestConfiguration.getFactory().close();
    }

}
