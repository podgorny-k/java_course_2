package org.levelup.first.maven;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@SuppressWarnings("ALL")
public class StringUtilsTest {

    // test<MethodName>_when<Input>_then<Result>
    // shouldReturnFalseIfStringIsNull
    @Test
    @DisplayName("isBlank(String): should return true if value is null")
    public void testIsBlank_whenStringIsNull_thenReturnTrue() {
        // given
        // when
        boolean result = StringUtils.isBlank(null);
        // then
        Assertions.assertTrue(result);
    }

    @Test
    public void testIsBlank_whenStringIsEmpty_thenReturnTrue() {
        // when
        boolean result = StringUtils.isBlank("");
        // then
        Assertions.assertTrue(result);
    }

    @Test
    public void testIsBlank_whenStringConsistsFromWhitespaces_thenReturnTrue() {
        // when
        boolean result = StringUtils.isBlank("     ");
        // then
        Assertions.assertTrue(result);
    }

    @Test
    public void testIsBlank_whenStringIsNotEmpty_thenReturnFalse() {
        // when
        boolean result = StringUtils.isBlank("some string");
        // then
        Assertions.assertFalse(result);
    }

}